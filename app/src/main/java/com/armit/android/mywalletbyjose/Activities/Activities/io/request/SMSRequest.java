package com.armit.android.mywalletbyjose.Activities.Activities.io.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jruelas on 26/05/17.
 */

public class SMSRequest {

    @SerializedName("Telefono")
    String Telefono;

    @SerializedName("OneSignalUserID")
    String OneSignalUserID;

    @SerializedName("OneSignalRegistrationID")
    String OneSignalRegistrationID;

    @SerializedName("CodigoValidacion")
    String CodigoValidacion;

    public SMSRequest(String Telefono, String OneSignalUserID, String OneSignalRegistrationID, String CodigoValidacion) {
        this.Telefono = Telefono;
        this.OneSignalUserID = OneSignalUserID;
        this.OneSignalRegistrationID = OneSignalRegistrationID;
        this.CodigoValidacion = CodigoValidacion;

    }

}
