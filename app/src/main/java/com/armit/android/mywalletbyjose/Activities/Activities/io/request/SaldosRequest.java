package com.armit.android.mywalletbyjose.Activities.Activities.io.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jruelas on 26/05/17.
 */

public class SaldosRequest {

        @SerializedName("TokenSeguridad")
        String TokenSeguridad;

        public  SaldosRequest(String TokenSeguridad){
            this.TokenSeguridad = TokenSeguridad;
        }

}
