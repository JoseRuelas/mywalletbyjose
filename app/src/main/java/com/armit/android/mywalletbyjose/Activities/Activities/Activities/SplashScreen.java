package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.armit.android.mywalletbyjose.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    SessionManager sessionManager;

    private static final long SPLASH_SCREEN_DELAY=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sessionManager = new SessionManager(getApplicationContext());



        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                sessionManager.checkLogin();
                finish();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task,SPLASH_SCREEN_DELAY);
    }



}
