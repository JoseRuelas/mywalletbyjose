package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.armit.android.mywalletbyjose.Activities.Activities.Adapter.RecyclerViewAdapter;
import com.armit.android.mywalletbyjose.Activities.Activities.beans.Operaciones;
import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiAdapter;
import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiService;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.SaldosRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.SaldosResponse;
import com.armit.android.mywalletbyjose.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    public List<Operaciones> operaciones;
    private ImageView sinMovimientos;
    private TextView txtSinMovimientos, txtSaldo;
    private SaldosRequest saldosRequest;
    private String tokenSeguridad;
    private SessionManager sessionManager;
    private  SaldosResponse saldosResponse;
    private String token;
    public static final String KEY_TOKEN= "token";
    private static final String PREF_NAME = "MyWalletToken";
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private ProgressDialog dialog;
    private Context _context;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean chequeoDialogo = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this._context = getApplicationContext();
        operaciones = new ArrayList<>();

        sessionManager = new SessionManager(getApplicationContext());

        sinMovimientos = (ImageView) findViewById(R.id.img_sinMovimientos);
        txtSinMovimientos = (TextView) findViewById(R.id.txt_sinMovimientos);
        txtSaldo = (TextView) findViewById(R.id.txt_saldo);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);



        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        obtenerDatosSaldos();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
         public void onRefresh() {
                    chequeoDialogo = true;
           // Refresh items
           refreshItems();
         }
        });

    }



    private void obtenerDatosSaldos()
    {

        if (chequeoDialogo == false) {
            dialog = new ProgressDialog(this, R.style.AppCompatProgressDialogStyle);
            dialog.setMessage("Cargando");
            dialog.setCancelable(false);
            dialog.setInverseBackgroundForced(false);
            dialog.show();
        }

        ApiService apiService = ApiAdapter.getClient().create(ApiService.class);
        saldosRequest = new SaldosRequest(loadTokenSeguridad());
        Call<SaldosResponse> call = apiService.getSaldos(saldosRequest);
        call.enqueue(new Callback<SaldosResponse>() {
            @Override
            public void onResponse(Call<SaldosResponse> call, Response<SaldosResponse> response) {
                dialog.dismiss();
                saldosResponse =  response.body();
                operaciones = saldosResponse.OPERACIONES;
                        if(!operaciones.isEmpty())
                        {
                            sinMovimientos.setVisibility(View.GONE);
                            txtSinMovimientos.setVisibility(View.GONE);
                            mAdapter = new RecyclerViewAdapter(operaciones);
                            mRecyclerView.setAdapter(mAdapter);
                            txtSaldo.setText(String.format("$ %.2f",Double.parseDouble(saldosResponse.getSaldo())));

                        }else
                        {
                            dialogoAlertaReiniciarSesion();

                        }
                  }

            @Override
            public void onFailure(Call<SaldosResponse> call, Throwable t) {
                dialog.dismiss();
                dialogoAlertaErrorConsulta();
            //Restart app
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }


        });


    }



    public String loadTokenSeguridad(){
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String token = preferences.getString(KEY_TOKEN, "TokenVacio");
        return  token;
    }

    public void borrarTokenSeguridad()
    {
        editor.remove(KEY_TOKEN);
        editor.commit();
    }


    void refreshItems() {
        // Load items
        // ...
       obtenerDatosSaldos();
        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...
        mAdapter.notifyDataSetChanged();

        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void dialogoAlertaReiniciarSesion()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Debes ingresar tus datos nuevamente.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sessionManager.cerrarlogin();
                        Intent intent = new Intent(MainActivity.this, Login.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaErrorConsulta()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Intentelo más tarde")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }



}
