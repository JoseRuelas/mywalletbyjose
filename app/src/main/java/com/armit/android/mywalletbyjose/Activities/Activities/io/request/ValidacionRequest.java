package com.armit.android.mywalletbyjose.Activities.Activities.io.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jruelas on 26/05/17.
 */

public class ValidacionRequest {

    @SerializedName("Telefono")
    @Expose
    String Telefono;

    public ValidacionRequest(String Telefono){
        this.Telefono = Telefono;
    }
}
