package com.armit.android.mywalletbyjose.Activities.Activities.io.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jruelas on 26/05/17.
 */

public class ResultadoRequest {

    @SerializedName("EsAutorizado")
    boolean EsAutorizado;

    @SerializedName("ID_Operacion")
    String ID_Operacion;

    public ResultadoRequest(boolean EsAutorizado, String ID_Operacion){
        this.EsAutorizado = EsAutorizado;
        this.ID_Operacion = ID_Operacion;
    }
}
