package com.armit.android.mywalletbyjose.Activities.Activities.io.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JoseAlberto on 24/05/2017.
 */

public class ValidacionResponse {

    @SerializedName("CodigoRespuesta")
    private String CodigoRespuesta;

    @SerializedName("Descripcion")
    private String Descripcion;

    @SerializedName("idUsuario")
    private String ID_Usuario;

    @SerializedName("TokenSeguridad")
    private String TokenSeguridad;


    public ValidacionResponse(String CodigoRespuesta, String Descripcion, String ID_Usuario, String TokenSeguridad){
        this.setCodigoRespuesta(CodigoRespuesta);
        this.Descripcion = Descripcion;
        this.ID_Usuario = ID_Usuario;
        this.TokenSeguridad  = TokenSeguridad;
    }

    public String getCodigoRespuesta() {
        return CodigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        CodigoRespuesta = codigoRespuesta;
    }
}
