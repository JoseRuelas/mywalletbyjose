package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiAdapter;
import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiService;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.SMSRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.SMSResponse;
import com.armit.android.mywalletbyjose.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SMS extends AppCompatActivity {

    private Button validar;
    private EditText codigo;
    private String sms;
    private SMSRequest smsRequest;
    private SessionManager sessionManager;
    private SMSResponse smsResponse;
    ProgressDialog dialog;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public static final String KEY_TOKEN= "token";
    private static final String PREF_NAME = "MyWalletToken";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        codigo = (EditText) findViewById(R.id.ed_sms);
        validar = (Button) findViewById(R.id.btn_validar);

        sessionManager = new SessionManager(getApplicationContext());

        validar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             sms = codigo.getText().toString();

                if(sms.isEmpty())
                {
                   dialogoAlertaVacio();
                }
               else if(sms.length()!=6)
               {
                   dialogoAlertaLongitud();
               }
             else
                {
                    obtenerDatosSMS();
                }
            }
        });

    }

    private void obtenerDatosSMS()
    {

        dialog = new ProgressDialog(this, R.style.AppCompatProgressDialogStyle);
        dialog.setMessage("Cargando");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.show();

        ApiService apiService = ApiAdapter.getClient().create(ApiService.class);
        smsRequest = new SMSRequest("telefono","OneSignalUserID","OneSignalRegistrationID","CodigoValidacion");
        Call<SMSResponse> call = apiService.getSMS(smsRequest);
        call.enqueue(new Callback<SMSResponse>() {
            @Override
            public void onResponse(Call<SMSResponse> call, Response<SMSResponse> response) {
                dialog.cancel();
                if (response.body().getCodigoRespuesta().equals("00"))
                {
                    sessionManager.createSMSSession(sms);
                    Log.v("SMS Response: ", "" + response.body());
                    Intent intent = new Intent(SMS.this, MainActivity.class);
                    saveTokenSeguridad(response.body().getTokenSeguridad());
                    startActivity(intent);
                    finish();
                }else
                    {
                        dialogoAlertaReiniciarSesion();
                    }
            }

            @Override
            public void onFailure(Call<SMSResponse> call, Throwable t) {
                dialog.cancel();
                dialogoAlertaErrorConsulta();
                //Restart app
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });
    }

    public void saveTokenSeguridad(String tokenSeguridad){

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor  = preferences.edit();
        editor.putString(KEY_TOKEN,tokenSeguridad);
        editor.commit();

    }

    public void dialogoAlertaLongitud()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Código erróneo")
                .setMessage("La longitud del código debe ser de 6 dígitos.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaVacio()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Código erróneo")
                .setMessage("Debes ingresar un código de 6 dígitos.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaErrorConsulta()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Intentelo más tarde")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaReiniciarSesion()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Debes ingresar tus datos nuevamente.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sessionManager.cerrarlogin();
                        Intent intent = new Intent(SMS.this, SMS.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

}
