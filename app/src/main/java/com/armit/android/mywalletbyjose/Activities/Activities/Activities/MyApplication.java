package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

import com.onesignal.OneSignal;

/**
 * Created by jruelas on 19/05/17.
 */

public class MyApplication extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new NotificationOpenedHandler())
                .setNotificationReceivedHandler(new NotificationReceivedHandler())

                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();


//        //NotificationHandler
//        OneSignal.startInit(this)
//                .setNotificationOpenedHandler(new NotificationOpenedHandler())
//                .init();
//
//        //NotificationReceived
//        OneSignal.startInit(this)
//                .setNotificationReceivedHandler(new NotificationReceivedHandler())
//                .init();

        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
