package com.armit.android.mywalletbyjose.Activities.Activities.io;

import com.armit.android.mywalletbyjose.Activities.Activities.io.request.ResultadoRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.SMSRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.SaldosRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.ValidacionRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.ResultadoResponse;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.SMSResponse;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.SaldosResponse;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.ValidacionResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by JoseAlberto on 24/05/2017.
 */

public interface ApiService {

    @POST("AUTORIZADOR_ValidaUsuario")
    Call<ValidacionResponse> getValidacion(@Body ValidacionRequest validacionRequest);

   @POST("AUTORIZADOR_ValidacionSMS")
    Call<SMSResponse> getSMS(@Body SMSRequest smsRequest);

    @POST("AUTORIZADOR_GetSaldosMovimientos")
    Call<SaldosResponse> getSaldos(@Body SaldosRequest saldosRequest);

    @POST("AUTORIZADOR_ResultadoTransaccion")
    Call<ResultadoResponse> getResultado(@Body ResultadoRequest resultadoRequest);


}
