package com.armit.android.mywalletbyjose.Activities.Activities.io.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JoseAlberto on 25/05/2017.
 */

public class SMSResponse {


    @SerializedName("CodigoRespuesta")
    private String CodigoRespuesta;

    @SerializedName("Descripcion")
    private String Descripcion;

    @SerializedName("ID_Usuario")
    private String ID_Usuario;

    @SerializedName("TokenSeguridad")
    private String TokenSeguridad;


    public SMSResponse(String CodigoRespuesta, String Descripcion, String ID_Usuario, String TokenSeguridad){
        this.setCodigoRespuesta(CodigoRespuesta);
        this.Descripcion = Descripcion;
        this.ID_Usuario = ID_Usuario;
        this.setTokenSeguridad(TokenSeguridad);
    }


    public String getTokenSeguridad() {
        return TokenSeguridad;
    }

    public void setTokenSeguridad(String tokenSeguridad) {
        TokenSeguridad = tokenSeguridad;
    }

    public String getCodigoRespuesta() {
        return CodigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        CodigoRespuesta = codigoRespuesta;
    }
}
