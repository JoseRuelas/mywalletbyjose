package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by jruelas on 19/05/17.
 */

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    private String numeroTransaccion;
    private String numeroTarjeta;
    private String monto;
    private String lugarTransaccion;

    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String customKey;


        if (data != null) {
            customKey = data.optString("customKey", "");
            numeroTransaccion = data.optString("numeroTransaccion", "");
            numeroTarjeta = data.optString("numeroTarjeta", "");
            monto = data.optString("monto", "");
            lugarTransaccion = data.optString("lugarTransaccion", "");
            if (customKey != null)
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
                Log.e("OneSignalExample", "additional data: " + result.notification.payload.additionalData);
        }

        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);

        // The following can be used to open an Activity of your choice.
        // Replace - getApplicationContext() - with any Android Context.
         Intent intent = new Intent(MyApplication.getContext(), Compras.class);
         intent.putExtra("NUM_TRANSACCION", numeroTransaccion);
         intent.putExtra("NUM_TARJETA" ,numeroTarjeta);
         intent.putExtra("MONTO",monto);
         intent.putExtra("LUGAR",lugarTransaccion);
         intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
         MyApplication.getContext().startActivity(intent);

        // Add the following to your AndroidManifest.xml to prevent the launching of your main Activity
        //   if you are calling startActivity above.
     /*
        <application ...>
          <meta-data android:name="com.onesignal.NotificationOpened.DEFAULT" android:value="DISABLE" />
        </application>
     */
    }
}
