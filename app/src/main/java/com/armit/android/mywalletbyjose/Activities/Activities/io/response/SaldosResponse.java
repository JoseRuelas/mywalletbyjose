package com.armit.android.mywalletbyjose.Activities.Activities.io.response;

import com.armit.android.mywalletbyjose.Activities.Activities.beans.Operaciones;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by JoseAlberto on 25/05/2017.
 */

public class SaldosResponse {


    @SerializedName("CodigoRespuesta")
    private String CodigoRespuesta;

    @SerializedName("Descripcion")
    public String Descripcion;

    @SerializedName("Saldo")
    private String Saldo;

    @SerializedName("OPERACIONES")
    public List<Operaciones> OPERACIONES;

    public SaldosResponse(String CodigoRespuesta, String Descripcion, String Saldo, List<Operaciones> OPERACIONES){
        this.setCodigoRespuesta(CodigoRespuesta);
        this.Descripcion = Descripcion;
        this.setSaldo(Saldo);
        this.OPERACIONES = OPERACIONES;
    }

    public String getSaldo() {
        return Saldo;
    }

    public void setSaldo(String saldo) {
        Saldo = saldo;
    }

    public String getCodigoRespuesta() {
        return CodigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        CodigoRespuesta = codigoRespuesta;
    }
}
