package com.armit.android.mywalletbyjose.Activities.Activities.Adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.armit.android.mywalletbyjose.Activities.Activities.beans.Operaciones;
import com.armit.android.mywalletbyjose.R;

import java.util.ArrayList;
import java.util.List;

import static android.support.constraint.R.id.parent;

/**
 * Created by jruelas on 23/05/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {


    private List<Operaciones> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_fecha;
        public ImageView img_tarjeta;
        public TextView txt_numTarjeta;
        public TextView txt_monto;
        public LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            txt_fecha = (TextView) itemView.findViewById(R.id.txt_date);
            img_tarjeta = (ImageView) itemView.findViewById(R.id.img_tarjeta);
            txt_numTarjeta = (TextView) itemView.findViewById(R.id.txt_numtarjeta);
            txt_monto = (TextView) itemView.findViewById(R.id.txt_monto_movimiento);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout_movimientos);

        }
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_movimientos, parent, false);
        RecyclerViewAdapter.ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        final Operaciones operaciones = mDataset.get(position);
        holder.txt_fecha.setText(operaciones.getFecha());
        String tipoTarjeta = operaciones.getTarjeta().substring(0,1);
        if(tipoTarjeta =="3")
            holder.img_tarjeta.setImageResource(R.drawable.ico_amex);
        else if(tipoTarjeta=="4")
            holder.img_tarjeta.setImageResource(R.drawable.ico_visa);
        else
            holder.img_tarjeta.setImageResource(R.drawable.ico_mastercard);
        String cuatroDigitos = String.format("**** **** **** %1$s", operaciones.getTarjeta().substring(operaciones.getTarjeta().length()-4));
        holder.txt_numTarjeta.setText(cuatroDigitos);
        holder.txt_monto.setText(String.format("%.2f",Double.parseDouble(operaciones.getImporte())));

    }



    public RecyclerViewAdapter (List<Operaciones> operaciones){mDataset = operaciones;}

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void updateAdapter()
    {
        mDataset.clear();


    }


}
