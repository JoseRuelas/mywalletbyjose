package com.armit.android.mywalletbyjose.Activities.Activities.io.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by JoseAlberto on 25/05/2017.
 */

public class ResultadoResponse {

    @SerializedName("CodigoRespuesta")
    private String CodigoRespuesta;

    @SerializedName("Descripcion")
    private String Descripcion;

    @SerializedName("ID_Usuario")
    private String ID_Usuario;

    @SerializedName("TokenSeguridad")
    private String TokenSeguridad;


    public ResultadoResponse(String CodigoRespuesta, String Descripcion, String ID_Usuario, String TokenSeguridad){
        this.setCodigoRespuesta(CodigoRespuesta);
        this.setDescripcion(Descripcion);
        this.ID_Usuario = ID_Usuario;
        this.TokenSeguridad  = TokenSeguridad;
    }

    public String getCodigoRespuesta() {
        return CodigoRespuesta;
    }

    public void setCodigoRespuesta(String codigoRespuesta) {
        CodigoRespuesta = codigoRespuesta;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
