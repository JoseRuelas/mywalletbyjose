package com.armit.android.mywalletbyjose.Activities.Activities.beans;

/**
 * Created by JoseAlberto on 25/05/2017.
 */

public class Operaciones {

    private String Tarjeta;
    private String Fecha;
    private String Sucursal;
    private String Afiliacion;
    private String Terminal;
    private String Operador;
    private String Autorizacion;
    private String Importe;
    private String Ticket;
    private String ID_Operacion;

    public Operaciones( String tarjeta, String fecha, String sucursal, String afiliacion, String terminal, String operador, String autorizacion, String importe, String ticket, String idOperacion)
    {
        this.Tarjeta = tarjeta;
        this.Fecha = fecha;
        this.Sucursal = sucursal;
        this.Afiliacion = afiliacion;
        this.Terminal = terminal;
        this.Operador = operador;
        this.Autorizacion = autorizacion;
        this.Importe = importe;
        this.Ticket = ticket;
        this.ID_Operacion = idOperacion;
    }

    public String getTarjeta() {
        return Tarjeta;
    }

    public String getFecha() {
        return Fecha;
    }

    public String getSucursal() {
        return Sucursal;
    }

    public String getAfiliacion() {
        return Afiliacion;
    }

    public String getTerminal() {
        return Terminal;
    }

    public String getOperador() {
        return Operador;
    }

    public String getAutorizacion() {
        return Autorizacion;
    }

    public String getImporte() {
        return Importe;
    }

    public String getTicket() {
        return Ticket;
    }

    public String getID_Operacion() {
        return ID_Operacion;
    }
}
