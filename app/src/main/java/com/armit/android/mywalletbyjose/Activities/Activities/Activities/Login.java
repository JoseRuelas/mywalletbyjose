package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiAdapter;
import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiService;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.ValidacionRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.ValidacionResponse;
import com.armit.android.mywalletbyjose.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    private String phone = "";
    private EditText phoneNumber;
    private ValidacionRequest validacionRequest;
    private SessionManager sessionManager;
    private ProgressDialog dialog;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = new SessionManager(getApplicationContext());
        Button ingresar = (Button) findViewById(R.id.btn_ingresar);
        phoneNumber = (EditText) findViewById(R.id.ed_phone);

        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phone = phoneNumber.getText().toString();

                if(phone.isEmpty())
                  {
                   dialogoAlertaVacio();
                  }
                else if (phone.length()!=10)
                {
                    dialogoAlertaLongitud();
                }else
                    {
                        obtenerDatosUsuario();
                    }
            }
        });
    }


    private void obtenerDatosUsuario()
    {

        dialog = new ProgressDialog(this, R.style.AppCompatProgressDialogStyle);
        dialog.setMessage("Cargando");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.show();

        ApiService apiService = ApiAdapter.getClient().create(ApiService.class);
        validacionRequest = new ValidacionRequest(phone);
        Call<ValidacionResponse> call = apiService.getValidacion(validacionRequest);
        call.enqueue(new Callback<ValidacionResponse>() {
            @Override
            public void onResponse(Call<ValidacionResponse> call, Response<ValidacionResponse> response) {
                dialog.cancel();
                Log.v("Validacion Response: ", "" + response.body());
                if (response.body().getCodigoRespuesta().equals("00") )
                {
                    sessionManager.createLoginSession(phone);
                    Intent intent = new Intent(Login.this, SMS.class);
                    startActivity(intent);
                    finish();
                }
                else
                    {
                        dialogoAlertaReiniciarSesion();
                    }

            }

            @Override
            public void onFailure(Call<ValidacionResponse> call, Throwable t) {
                dialog.cancel();
                dialogoAlertaErrorConsulta();
                //Restart app
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });
    }

    public void dialogoAlertaLongitud()
    {
        AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Teléfono erróneo")
                .setMessage("La longitud del número debe ser de 10 dígitos.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaVacio()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Teléfono erróneo")
                .setMessage("Debes ingresar un número teléfonico de 10 dígitos.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaErrorConsulta()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Intentelo más tarde")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaReiniciarSesion()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Debes ingresar tus datos nuevamente.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sessionManager.cerrarlogin();
                        Intent intent = new Intent(Login.this, Login.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }
}
