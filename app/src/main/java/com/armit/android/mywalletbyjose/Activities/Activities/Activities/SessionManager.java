package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static android.os.ParcelFileDescriptor.MODE_WORLD_READABLE;

/**
 * Created by jruelas on 30/05/17.
 */

public class SessionManager {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "MyWalletLogin";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_PHONE = "phone";

    // Email address (make variable public to access from outside)
    public static final String KEY_CODE= "codigo";




    //Constructor

    public SessionManager(Context context)
    {
        this._context = context;
        preferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor  = preferences.edit();
    }

    public void createLoginSession(String phone)
    {
        // Storing name in pref
        editor.putString(KEY_PHONE, phone);

        // commit changes
        editor.commit();
    }

    public void createSMSSession(String codigo)
    {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing codigo in pref
        editor.putString(KEY_CODE, codigo);

        // commit changes
        editor.commit();
    }

    public void checkLogin()
    {
        if(this.isLoggedIn())
        {
            Intent intent = new Intent(_context, MainActivity.class);
            _context.startActivity(intent);
        } else {
            // user is not logged in redirect him to Login Activity
            Intent intent = new Intent(_context, Login.class);

            // Closing all the Activities
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(intent);
        }
    }

    //Get Login State
    public boolean isLoggedIn()
    {
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void cerrarlogin()
    {
      editor.remove(KEY_PHONE);
      editor.remove(KEY_CODE);
        editor.commit();
    }

}
