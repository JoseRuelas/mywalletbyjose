package com.armit.android.mywalletbyjose.Activities.Activities.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.armit.android.mywalletbyjose.Activities.Activities.beans.Operaciones;
import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiAdapter;
import com.armit.android.mywalletbyjose.Activities.Activities.io.ApiService;
import com.armit.android.mywalletbyjose.Activities.Activities.io.request.ResultadoRequest;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.ResultadoResponse;
import com.armit.android.mywalletbyjose.Activities.Activities.io.response.SaldosResponse;
import com.armit.android.mywalletbyjose.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Compras extends AppCompatActivity {

    private Button btn_aceptar,btn_rechazar;
    private TextView txtCompras;
    private boolean resultado;
    private ResultadoRequest resultadoRequest;
    private boolean esAutorizado;
    private String idOperacion;
    private  ProgressDialog dialog;
    private SaldosResponse saldosResponse;
    private Operaciones operaciones;
    private String numeroTransaccion;
    private String numeroTarjeta;
    private String monto;
    private String lugarTransaccion;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compras);

        btn_aceptar = (Button) findViewById(R.id.btn_aceptar);
        btn_rechazar = (Button) findViewById(R.id.btn_rechazar);
        txtCompras = (TextView) findViewById(R.id.txt_compras);

        numeroTransaccion = getIntent().getStringExtra("NUM_TRANSACCION");
        numeroTarjeta = getIntent().getStringExtra("NUM_TARJETA");
        monto = getIntent().getStringExtra("MONTO");
        lugarTransaccion = getIntent().getStringExtra("LUGAR");
        txtCompras.setText(String.format(getString(R.string.txt_compras),numeroTarjeta,lugarTransaccion,monto));

        btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                esAutorizado = true;
                idOperacion = numeroTransaccion;
                obtenerDatosResultado();
            }
        });

        btn_rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                esAutorizado = false;
                idOperacion = numeroTransaccion;
                obtenerDatosResultado();
            }
        });


    }

    private void obtenerDatosResultado()
    {


            dialog = new ProgressDialog(this,  R.style.AppCompatProgressDialogStyle);
            dialog.setMessage("Cargando");
            dialog.setCancelable(false);
            dialog.setInverseBackgroundForced(false);
            dialog.show();

        ApiService apiService = ApiAdapter.getClient().create(ApiService.class);
        resultadoRequest = new ResultadoRequest(esAutorizado,idOperacion);
        Call<ResultadoResponse> call = apiService.getResultado(resultadoRequest);
        call.enqueue(new Callback<ResultadoResponse>() {
            @Override
            public void onResponse(Call<ResultadoResponse> call, Response<ResultadoResponse> response) {
                dialog.dismiss();
                if (esAutorizado = true)
                {
                  if(response.body().getCodigoRespuesta().equals("04"))
                    {
                        dialogoAlertaCompraRechazada(response.body().getDescripcion());
                    }
                    else if (response.body().getCodigoRespuesta().equals("06"))
                    {
                        dialogoAlertaCompraRechazada(response.body().getDescripcion());
                    }
                    else{
                            dialogoAlertaCompraRealizada(response.body().getDescripcion());
                        }
                }else if (esAutorizado = false)
                {
                    dialogoAlertaCompraRechazada(response.body().getDescripcion());
                }
                else if (!response.body().getCodigoRespuesta().equals("00"))
                {
                    dialogoAlertaReiniciarSesion();
                }
            }

            @Override
            public void onFailure(Call<ResultadoResponse> call, Throwable t) {
                dialog.dismiss();
                dialogoAlertaErrorConsulta();
                //Restart app
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });
    }

    public void dialogoAlertaErrorConsulta()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Intentelo más tarde")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }

    public void dialogoAlertaCompraRealizada(String response)
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Compra realizada con exito.")
                .setMessage(String.valueOf(response))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Compras.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setIcon(R.drawable.ico_accept)
                .show();

    }

    public void dialogoAlertaCompraRechazada(String response)
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Compra rechazada.")
                .setMessage(String.valueOf(response))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        Intent intent = new Intent(Compras.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();

    }

    public void dialogoAlertaReiniciarSesion()
    {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        builder.setTitle("Error en consulta de datos")
                .setMessage("Debes ingresar tus datos nuevamente.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sessionManager.cerrarlogin();
                        Intent intent = new Intent(Compras.this, Compras.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setIcon(R.drawable.ico_denied)
                .show();
    }
}
